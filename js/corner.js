$(document).ready(function(){
    $(".project").bind("mouseenter", function(){
        $( this ).find(".whiteout").stop().animate({
            top:0, 
            left:0
        }, 400);
    }).bind("mouseleave", function(){
        $( this ).find(".whiteout").stop().animate({
            top:-200, 
            left:-200
        }, 200);
    })
});
